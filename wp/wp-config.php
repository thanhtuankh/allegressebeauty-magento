<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'zdb_allegressebeauty');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lf-V]EEVbmR^dB?o<6Pienz5eS5,uPQq/8S^GFgg rzZ){nt>;X<{zW8V<v,O|=m');
define('SECURE_AUTH_KEY',  ')hBxt!SgCV+T7_*K|8,HeHY4%yr-7;hvD~2j?d*[)O6Qpb}`Iwc[_WN;T)PU2)iO');
define('LOGGED_IN_KEY',    'HX^$Z^>0=3l:%O|kvj<ynd<;Rw2X>:C4iuxPm?:lL9;8_q194g&_)JCm>:Y0*_7X');
define('NONCE_KEY',        'Kv hpMD=`0ABP0y-]7M!3#aso=lec-__X4UIK}Wno;<MT;Q!6;<CX4/{]S^1Mxt]');
define('AUTH_SALT',        'WNE@`f;hi+mjyf8uY7-Gyx;vNqJMXM4?sC%{@Xgq2,&rWL.[YG<EN~V)0*{AJpO[');
define('SECURE_AUTH_SALT', 'psdVCqXvq|Ddr?57ej`KZ99{LgOk)JYy7`qo%& 2XozK3zOA%+QcAqo@if5BS1>u');
define('LOGGED_IN_SALT',   '6EUw^h0Gp/;!t-Tp-_{Zma~Q3%=J(<s@eF<;_L))e^Rpr65&ur<[#xd$]fYRC$/{');
define('NONCE_SALT',       'fCQq}5CK8;9R]1KoV,6X=|>xIaPkd7-si I1,lGL1_dsi45jgqF4#0 mZ@&Lc<F=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
