var $j = jQuery.noConflict();
$j(window).load(function(){
  if ($j(".zoom").length > 0) $j(".zoom").anythingZoomer();
    var thumbs = $j(".showcase-thumbnail-wrapper > div");
    thumbs.each(function(i){
      $j(thumbs[i]).click(function(){
        setTimeout(function () { 
          $j(".zoom").anythingZoomer();        
      }, 50);
    });
	 
  });  
   
$j( '.accordion_holder' ).accordion({
      heightStyle: "content",
      collapsible: true,
      active:false
    });
	var slider_pager_w = $j("#slider .bx-pager").width();
	$j("#slider .bx-pager").css('margin-left', -slider_pager_w/2);
});
	
$j(document).ready(function() {


	$j('.for_jscroll_pane').jScrollPane();

	$j('.select_holder select').sSelect();
	$j('.select_holder_product_details select').sSelect().change(function(){
		
	});

	$j('.select_holder_wholesale select').sSelect();
	$j('.warranty_form_select select').sSelect();

	if ($j('input[type="checkbox"]').length > 0) $j('input[type="checkbox"]').ezMark();
	if ($j('input[type="radio"]').length > 0) $j('input[type="radio"]').ezMark();


	placeholderReplace();
			  
  $j('.bxslider').bxSlider({
	mode: 'fade',
	captions: true,
	controls:false,
	auto:true,
	pause:4000
  	});


$j('.products').bxSlider({

	minSlides: 5,
	maxSlides: 5,
	slideWidth: 191,
	slideMargin: 0,
	pager:false,
	moveSlides:1,
	hideControlOnEnd: true,
	infiniteLoop: false
	});
	


/* $j("nav ul li.shop-li").on("mouseenter", function(){
    $j(this).find('.menu_iner').css('display','block');
  	$j('.scroll_pane').jScrollPane();
 });

$j("nav ul li").on("mouseleave", function(){
    $j(this).find('.menu_iner').css('display','none');
   
 }); */

$j('nav ul li').has('.menu_iner').addClass('has_dropdown');

$j('.has_dropdown').hover(function(){
  $j(this).addClass('top_menu_arrow')},
  function(){
    $j(this).removeClass('top_menu_arrow')
 });



	
	var service_nav_width=960-$j('.top_nav_service_pages ul').width();
	var x=service_nav_width/2;
	$j('.top_nav_service_pages ul').css('padding-left',x);  

	$j(".showcase").awShowcase(
      {
        content_width:      390,
        content_height:     290,
        fit_to_parent:      false,
        auto:         false,
        interval:       3000,
        continuous:       false,
        loading:        true,
        tooltip_width:      200,
        tooltip_icon_width:   32,
        tooltip_icon_height:  32,
        tooltip_offsetx:    18,
        tooltip_offsety:    0,
        arrows:         true,
        buttons:        false,
        btn_numbers:      false,
        keybord_keys:     false,
        mousetrace:       false, /* Trace x and y coordinates for the mouse */
        pauseonover:      true,
        stoponclick:      true,
        transition:       'fade', /* hslide/vslide/fade */
        transition_delay:   300,
        transition_speed:   500,
        show_caption:     'onhover', /* onload/onhover/show */
        thumbnails:       true,
        thumbnails_position:  'outside-last', /* outside-last/outside-first/inside-last/inside-first */
        thumbnails_direction: 'vertical', /* vertical/horizontal */
        thumbnails_slidex:    1, /* 0 = auto / 1 = slide one thumbnail / 2 = slide two thumbnails / etc. */
        dynamic_height:     false, /* For dynamic height to work in webkit you need to set the width and height of images in the source. Usually works to only set the dimension of the first slide in the showcase. */
        speed_change:     false, /* Set to true to prevent users from swithing more then one slide at once. */
        viewline:       false /* If set to true content_width, thumbnails, transition and dynamic_height will be disabled. As for dynamic height you need to set the width and height of images in the source. */
      });
      
      
  $j('.fancybox-zoom').fancybox();
  
  $j('a.click-to-fancy').click(function(e) {
		e.preventDefault();
		$j(".fancybox-zoom").click();
    });

	$j("ul.tabs").tabs("div.right_welcome_product_details > div", {
		onClick: function() {
			$j('.for_jscroll_pane').jScrollPane();
		}
    });
	
	$j(".menu_iner ul").tabs("div.categories_header_holder > div", {
		onClick: function(event, tabIndex) {
			var top_nav_slider_item_number = $j($j('.container_slider')[tabIndex]).find('.top_nav_slider_item').length;
			$j($j('.container_slider')[tabIndex]).css('width',top_nav_slider_item_number*223);
			$j($j('.scroll_pane')[tabIndex]).jScrollPane({autoReinitialise: true});
		}
      });

 });
function placeholderReplace(){
    $j('[placeholder]').focus(function() {
     var input = $j(this);
      if (input.val() == input.attr('placeholder')) {
        if (this.originalType) {
          this.type = this.originalType;
          delete this.originalType;
        }
        input.val('');
        input.removeClass('placeholder');
      }
    }).blur(function() {
      var input = $j(this);
      if (input.val() == '') {
        if (this.type == 'password') {
          this.originalType = this.type;
          this.type = 'text';
        }
        input.addClass('placeholder');
        input.val(input.attr('placeholder'));
      }
    }).blur();
}
