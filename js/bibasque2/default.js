jQuery(document).ready(function($){
  $( '#home-slider' ).cycle({
	 "slides" : "> a",
	 "pause-on-hover" : true,
	 "fx" : "fadeout",
	 "speed" : 1500,
	 "timeout" : 4000,
	 "manual-speed" : 200
  });
  
  if ($('.accordion_holder').length > 0) {
	  $('.accordion_holder .accordion_text_holder').hide();
	  $('.accordion_holder h3').click(function () {
		  var that = this;
		  $('.accordion_holder .accordion_text_holder').each(function () {
			  if (!$(this).is($(that).next())) {
				  if ($(this).is(':visible')) {
					  $(this).slideUp();
				  }
			  }
		  });
		  $(this).next().slideToggle();
	  });
  }
  
  if ($('.tabs').length > 0) {
    $('.tabs li').click(function () {
        var i = $(this).index('.tabs li');
        $('.right_welcome_product_details > div').removeClass('active');
        $('.right_welcome_product_details > div:eq(' + i + ')').addClass('active');
        $('.tabs li').removeClass('active');
        $(this).addClass('active');
        return false;
    });
  }
  
});
jQuery(window).load(function () {
    if (jQuery('.category_all').length > 0) {
        var highestBox = 0;
        jQuery('.category_all li').each(function(){  
            if (jQuery(this).height() > highestBox) 
                   highestBox = jQuery(this).height(); 
            
            
        });
        jQuery('.category_all li').height(highestBox);
    }
});
