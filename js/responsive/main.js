jQuery(document).ready(function($){
	if($('#category-slider').length){
		$('#category-slider').slick({
			infinite: true,
			arrows: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			autoplay: true,
  			autoplaySpeed: 5000,
			responsive: [
			    {
			      breakpoint: 1025,
			      settings: {
			        arrows: false,
			        dots:true,
			        slidesToShow: 3
			      }
			    },
			    {
			      breakpoint: 715,
			      settings: {
			      	arrows: false,
			      	dots:true,
			        slidesToShow: 2
			      }
			    },
			    {
			      breakpoint: 481,
			      settings: {
			      	arrows: false,
			      	dots:true,
			        slidesToShow: 1
			      }
			    }
			]
		});
	}
	if($('#category-slider2').length){
		$('#category-slider2').slick({
			infinite: true,
			arrows: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			autoplay: true,
  			autoplaySpeed: 5000,
			responsive: [
			    {
			      breakpoint: 1025,
			      settings: {
			        arrows: false,
			        dots:true,
			        slidesToShow: 3
			      }
			    },
			    {
			      breakpoint: 715,
			      settings: {
			      	arrows: false,
			      	dots:true,
			        slidesToShow: 2
			      }
			    },
			    {
			      breakpoint: 481,
			      settings: {
			      	arrows: false,
			      	dots:true,
			        slidesToShow: 1
			      }
			    }
			]
		});
	}

	$("#header .icon-menu").click(function(){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			$(this).parent().find(".main-menu").hide();
		}else{
			$(this).parent().find(".main-menu").show();
			$(this).addClass("active");
		}
	});
});
