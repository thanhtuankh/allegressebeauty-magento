<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
date_default_timezone_set('America/Los_Angeles');

if (!in_array($_SERVER['REMOTE_ADDR'], array('195.24.147.122'))) {
    die('unauthorized');
}

set_time_limit(0);


if (isset($_REQUEST['backup'])) {
    $name = $_SERVER['SERVER_NAME'];
    $name.= date('YdmHi');
    `tar -zcvf ../{$name}.tar.gz ./`;
    
    $xml = simplexml_load_string(file_get_contents('app/etc/local.xml'));
    $user = (string)($xml->global[0]->resources[0]->default_setup[0]->connection[0]->username);
    $password = (string)($xml->global[0]->resources[0]->default_setup[0]->connection[0]->password);
    $dbname = (string)($xml->global[0]->resources[0]->default_setup[0]->connection[0]->dbname);
    
    $cmd = 'mysqldump --create-options -u' . $user . ' -p"' . $password . '" ' . $dbname . ' | gzip > ../' . $name . '.sql.gz';
    `$cmd`;
    
    print 'done!<br><a href="backup_magento.php">Back</a>';
    exit;
}

$dh = opendir('..');
if (!$dh) {
    die("Can't read parent dir");
}

$archives = array();
while ($file = readdir($dh)) {
    if (pathinfo($file, PATHINFO_EXTENSION) == 'gz') {
        $archives[] = $file;
    }
}
sort($archives);

print '<ul>';
foreach ($archives as $a) {
    print '<li>' . htmlspecialchars($a) . '</li>';
}
print '</ul>';

print '<br/><br/><a href="?backup=1">Backup</a>';
?>
