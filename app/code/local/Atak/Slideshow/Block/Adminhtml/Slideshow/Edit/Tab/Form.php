<?php

class Atak_Slideshow_Block_Adminhtml_Slideshow_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('slideshow_form', array('legend'=>Mage::helper('slideshow')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('slideshow')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
		));
	
		$fieldset->addField('slide_image', 'image', array(
			'label'     => Mage::helper('slideshow')->__('Image'),
			'required'  => false,
			'name'      => 'slide_image',
		));
	
		$fieldset->addField('status', 'select', array(
			'label'     => Mage::helper('slideshow')->__('Status'),
			'name'      => 'status',
			'values'    => array(
				array(
					'value'     => 1,
					'label'     => Mage::helper('slideshow')->__('Enabled'),
				),
				array(
					'value'     => 2,
					'label'     => Mage::helper('slideshow')->__('Disabled'),
				),
			),
		));
     
        
      if ( Mage::getSingleton('adminhtml/session')->getSlideshowData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getSlideshowData());
          Mage::getSingleton('adminhtml/session')->setSlideshowData(null);
      } elseif ( Mage::registry('slideshow_data') ) {
          $form->setValues(Mage::registry('slideshow_data')->getData());
      }
      return parent::_prepareForm();
  }
}