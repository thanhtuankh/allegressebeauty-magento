<?php
class Atak_Slideshow_Block_Slideshow extends Mage_Core_Block_Template
{
	public function __construct() {
		parent::__construct();
		$collection = Mage::getModel('slideshow/slideshow')->getCollection();
		$collection->setOrder('slide_id', 'ASC');
		$collection->addFieldToFilter('status',1);
		$this->setSlideshow($collection);
		
	}
	public function _prepareLayout()
    {
		
		return parent::_prepareLayout();
    }
    
     public function getSlideshow()     
     { 
        if (!$this->hasData('slideshow')) {
            $this->setData('slideshow', Mage::registry('slideshow'));
        }
        return $this->getData('slideshow');
        
    }
}