<?php
class Atak_Form_IndexController extends Mage_Core_Controller_Front_Action
{
	
	public function indexAction(){
		if($this->getRequest()->isPost()){
			$params = $this->getRequest()->getParams();
			$bodytext  =  "Full Name: " . $params['name'] . "\n";
			$bodytext .=  "Company Name: " . $params['companyname'] . "\n";
			$bodytext .=  "Email: " . $params['email'] . "\n";
			$bodytext .=  "Phone: " . $params['phone'] . "\n";
			$bodytext .=  "Subject: " . $params['subject'] . "\n";
			$bodytext .=  "Message: " . $params['message'];
			$mail = new Zend_Mail();
			$sender_name = Mage::getStoreConfig('trans_email/ident_general/name'); //sender name
			$sender_email = "wholesale@bibasque.com"; //sender email
			$mail->setBodyText($bodytext);
			$mail->setFrom($params['email']);
			$mail->addTo($sender_email, $sender_name);
			$mail->setSubject('Wholesale Contact form');
			try {
				$mail->send();
				Mage::getSingleton('customer/session')->addSuccess('Message sent successfully');
				$this->_redirect('message-sent');
			}
			catch(Exception $ex) {
				Mage::getSingleton('core/session')->addError('Unable to send email.');
				session_write_close();
				$this->_redirect('about-bibasque/wholesale');
			}
		}
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
        $this->renderLayout();
		
	}
}
?>