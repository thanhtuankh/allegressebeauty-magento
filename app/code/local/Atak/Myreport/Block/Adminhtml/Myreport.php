<?php
class Atak_Myreport_Block_Adminhtml_Myreport extends Mage_Adminhtml_Block_Widget_Grid_Container {
 
    public function __construct() {
        $this->_controller = 'adminhtml_myreport';
        $this->_blockGroup = 'myreport';
        $this->_headerText = Mage::helper('myreport')->__('Report');
        parent::__construct();
        $this->_removeButton('add');
    }
 
}