<?php
class Atak_Myreport_Block_Adminhtml_Myreport_Grid extends Mage_Adminhtml_Block_Report_Grid {
 
    public function __construct() {
        parent::__construct();
        $this->setId('myreportGrid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setSubReportSize(false);
    }
 
    protected function _prepareCollection() {
        parent::_prepareCollection();
        $this->getCollection()->initReport('myreport/myreport');
        return $this;
    }
 
    protected function _prepareColumns() {
        $this->addColumn('ordered_qty', array(
            'header'    =>Mage::helper('reports')->__('Qty'),
            'align'     =>'right',
            'index'     =>'ordered_qty',
            'total'     =>'sum',
            'type'      =>'number'
        ));
		$this->addColumn('created_at', array(
            'header'    =>Mage::helper('myreport')->__('Date'),
            'align'     =>'right',
            'index'     =>'created_at',
          
        ));
		$this->addColumn('sales_increment_id', array(
            'header'    =>Mage::helper('myreport')->__('Order Id'),
            'align'     =>'right',
            'index'     =>'sales_increment_id',
			'filter_index' => 'i.sales_increment_id',
            'type'      =>'number'
          
        ));
		$this->addColumn('status', array(
            'header'    =>Mage::helper('myreport')->__('Status'),
            'align'     =>'left',
            'index'     =>'status',
            'type'      =>'text'
        ));
		$this->addColumn('customer_firstname', array(
            'header'    =>Mage::helper('myreport')->__('First Name'),
            'align'     =>'left',
            'index'     =>'customer_firstname',
            'type'      =>'text'
        ));
		$this->addColumn('customer_lastname', array(
            'header'    =>Mage::helper('myreport')->__('Last Name'),
            'align'     =>'left',
            'index'     =>'customer_lastname',
            'type'      =>'text'
        ));
		$this->addColumn('customer_email', array(
            'header'    =>Mage::helper('myreport')->__('Email'),
            'align'     =>'left',
            'index'     =>'customer_email',
            'type'      =>'text'
        ));
		$this->addColumn('subtotal', array(
            'header'    =>Mage::helper('myreport')->__('Sales'),
            'align'     =>'right',
            'index'     =>'subtotal'
			
        ));
		$this->addColumn('shipping_amount', array(
            'header'    =>Mage::helper('myreport')->__('Shipping and Handling Amount'),
            'align'     =>'right',
            'index'     =>'shipping_amount'
			
        ));
		$this->addColumn('base_discount_amount', array(
            'header'    =>Mage::helper('myreport')->__('Discount'),
            'align'     =>'right',
            'index'     =>'base_discount_amount'
			
        ));
		$this->addColumn('grand_total', array(
            'header'    =>Mage::helper('myreport')->__('Gros'),
            'align'     =>'right',
            'index'     =>'grand_total'
			
        ));
		$this->addColumn('method', array(
            'header'    =>Mage::helper('myreport')->__('Payment Type'),
            'align'     =>'right',
            'index'     =>'method'
			
        ));
		$this->addColumn('status', array(
            'header'    =>Mage::helper('myreport')->__('Status'),
            'align'     =>'right',
            'index'     =>'status',
            'type'      =>'text'
        ));
		$this->addColumn('sales_customer_id', array(
            'header'    =>Mage::helper('myreport')->__('Customer ID'),
            'align'     =>'right',
            'index'     =>'sales_customer_id',
			'filter_index' => 'i.sales_customer_id',
            'type'      =>'number'
        ));
		$this->addColumn('invoice_increment_id', array(
            'header'    =>Mage::helper('myreport')->__('Invoice Number'),
            'align'     =>'right',
            'index'     =>'invoice_increment_id',
			'filter_index' => 'sl.invoice_increment_id',
            'type'      =>'number'
        ));
		$this->addColumn('name', array(
            'header'    =>Mage::helper('myreport')->__('Name'),
            'align'     =>'right',
            'index'     =>'name',
			'filter_index' => 'sl.name'
        ));
        $this->addColumn('item_id', array(
            'header' => Mage::helper('myreport')->__('Item ID'),
            'align' => 'right',
            'index' => 'item_id',
            'type'  => 'number'
        ));
		$this->addColumn('name', array(
            'header'    =>Mage::helper('myreport')->__('Name'),
            'align'     =>'right',
            'index'     =>'name',
			'filter_index' => 'sl.name'
        ));
		$this->addColumn('name', array(
            'header'    =>Mage::helper('myreport')->__('Name'),
            'align'     =>'right',
            'index'     =>'name',
			'filter_index' => 'sl.name'
        ));
		$this->addColumn('b_street', array(
            'header'    =>Mage::helper('myreport')->__('Billing Street'),
            'align'     =>'right',
            'index'     =>'b_street',
			'filter_index' => 'sa.b_street'
        ));
		$this->addColumn('b_country_id', array(
            'header'    =>Mage::helper('myreport')->__('Billing Country'),
            'align'     =>'right',
            'index'     =>'b_country_id',
			'filter_index' => 'sa.b_country_id'
        ));
		$this->addColumn('b_region', array(
            'header'    =>Mage::helper('myreport')->__('Billing State'),
            'align'     =>'right',
            'index'     =>'b_region',
			'filter_index' => 'sa.b_region'
        ));
		$this->addColumn('b_city', array(
            'header'    =>Mage::helper('myreport')->__('Billing City'),
            'align'     =>'right',
            'index'     =>'b_city',
			'filter_index' => 'sa.b_city'
        ));
		$this->addColumn('b_telephone', array(
            'header'    =>Mage::helper('myreport')->__('Billing Phone'),
            'align'     =>'right',
            'index'     =>'b_telephone',
			'filter_index' => 'sa.b_telephone'
        ));
		$this->addColumn('s_street', array(
            'header'    =>Mage::helper('myreport')->__('Shipping Street'),
            'align'     =>'right',
            'index'     =>'s_street',
			'filter_index' => 'sas.s_street'
        ));
		$this->addColumn('s_country_id', array(
            'header'    =>Mage::helper('myreport')->__('Shipping Country'),
            'align'     =>'right',
            'index'     =>'s_country_id',
			'filter_index' => 'sas.s_country_id'
        ));
		$this->addColumn('s_region', array(
            'header'    =>Mage::helper('myreport')->__('Shipping State'),
            'align'     =>'right',
            'index'     =>'s_region',
			'filter_index' => 'sas.s_region'
        ));
		$this->addColumn('s_city', array(
            'header'    =>Mage::helper('myreport')->__('Shipping City'),
            'align'     =>'right',
            'index'     =>'s_city',
			'filter_index' => 'sas.s_city'
        ));
		$this->addColumn('gift_code', array(
            'header'    =>Mage::helper('myreport')->__('Voucher'),
            'align'     =>'right',
            'index'     =>'gift_code',
			'filter_index' => 'gvc.gift_code'
            
        ));
		$this->addExportType('*/*/exportCsv', Mage::helper('myreport')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('myreport')->__('XML'));
        return parent::_prepareColumns();
    }
 
    public function getRowUrl($row) {
        return false;
    }
 
    public function getReport($from, $to) {
        if ($from == '') {
            $from = $this->getFilter('report_from');
        }
        if ($to == '') {
            $to = $this->getFilter('report_to');
        }
        $totalObj = Mage::getModel('reports/totals');
        $totals = $totalObj->countTotals($this, $from, $to);
        $this->setTotals($totals);
        $this->addGrandTotals($totals);
        return $this->getCollection()->getReport($from, $to);
    }
}