<?php
class Atak_MyReport_Block_MyReport extends Mage_Core_Block_Template {
 
    public function _prepareLayout() {
        return parent::_prepareLayout();
    }
 
    public function getReport() {
        if (!$this->hasData('myreport')) {
            $this->setData('myreport', Mage::registry('myreport'));
        }
        return $this->getData('myreport');
    }
 
}