<?php
class Atak_Myreport_Model_Myreport extends Mage_Reports_Model_Mysql4_Order_Collection
{
    function __construct() {
        parent::__construct();
      //  $this->setResourceModel('sales/order_item');
        $this->_init('sales/order_item','item_id');
   }
 
    public function setDateRange($from, $to) {
        $this->_reset();
        $this->getSelect()
             ->joinInner(array(
                 'i' => $this->getTable('sales/order_item')),
                 'i.order_id = main_table.entity_id',array('main_table.increment_id as sales_increment_id', 'main_table.customer_id as sales_customer_id')
                 )
             ->where('i.parent_item_id is null')
             ->where("i.created_at BETWEEN '".$from."' AND '".$to."'")
             ->columns(array('ordered_qty' => 'count(distinct `main_table`.`entity_id`)'))
			 ->joinLeft(array(
                 'sh' => Mage::getSingleton('core/resource')->getTableName('sales_flat_shipment')),
                 'sh.order_id = main_table.entity_id'
                 )
				->joinLeft(array(
                 'sl' => Mage::getSingleton('core/resource')->getTableName('sales_flat_invoice')),
                 'sl.order_id = main_table.entity_id',array('sl.increment_id as invoice_increment_id')
                 )
				 ->joinLeft(array(
                 'pm' => Mage::getSingleton('core/resource')->getTableName('sales_flat_order_payment')),
                 'pm.entity_id = main_table.entity_id'
                 ) 
				 ->joinLeft(array(
                 'sa' => Mage::getSingleton('core/resource')->getTableName('sales_flat_order_address')),
                 'sa.entity_id = main_table.billing_address_id',array('sa.street as b_street', 'sa.country_id as b_country_id', 'sa.region as b_region', 'sa.city as b_city', 'sa.telephone as b_telephone')
                 )
				  ->joinLeft(array(
                 'sas' => Mage::getSingleton('core/resource')->getTableName('sales_flat_order_address')),
                 'sas.entity_id = main_table.shipping_address_id',array('sas.street as s_street', 'sas.country_id as s_country_id', 'sas.region as s_region', 'sas.city as s_city', 'sas.telephone as s_telephone')
                 )
				 ->joinLeft(array(
                 'gv' => Mage::getSingleton('core/resource')->getTableName('giftvoucher_history')),
                 'gv.order_increment_id = main_table.increment_id',array('gv.giftvoucher_id as giftvoucher_id')
                 )
				 ->joinLeft(array(
                 'gvc' => Mage::getSingleton('core/resource')->getTableName('giftvoucher')),
                 'gvc.giftvoucher_id = gv.giftvoucher_id',array('gvc.gift_code as gift_code')
                 )
				 ->joinLeft(array(
                 'oi' => Mage::getSingleton('core/resource')->getTableName('sales_flat_order_item')),
                 'oi.order_id = main_table.entity_id'
                 )
				 ->where('oi.parent_item_id is null')
				 ->group(array('oi.name', 'oi.order_id'));
				;
			    // uncomment next line to get the query log:
		
		
     //  echo  $this->getSelect()->__toString();
        return $this;
    }
 
    public function setStoreIds($storeIds)
    {
        return $this;
    }
 
}
?>