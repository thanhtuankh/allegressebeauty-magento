<?php class Atak_Voucher_Model_Observer {

	public function submitVoucher($observer) {

		 $session = Mage::getSingleton('customer/session');
        if (!$session->isLoggedIn()) {
            return array();
        }
        $customerId = $session->getCustomer()->getId();
        $collection = Mage::getResourceModel('giftvoucher/customervoucher_collection')
            ->addFieldToFilter('main_table.customer_id', $customerId);
        $voucherTable = $collection->getTable('giftvoucher/giftvoucher');
        $collection->getSelect()
            ->join(array('v' => $voucherTable),
                'main_table.voucher_id = v.giftvoucher_id',
                array('gift_code', 'balance', 'currency', 'conditions_serialized', 'giftvoucher_comments', 'free_shipping')
            )->where('v.status = ?', Magestore_Giftvoucher_Model_Status::STATUS_ACTIVE)
            ->where("v.recipient_name IS NULL OR v.recipient_name = '' OR (v.customer_id <> '" .
                $customerId . "' AND v.customer_email <> ?)",
                $session->getCustomer()->getEmail()
            );

        $giftCards = array();
        $addedCodes = array();
        if ($codes = Mage::getSingleton('checkout/session')->getGiftCodes()) {
            $addedCodes = explode(',', $codes);
        }
        $helper = Mage::helper('giftvoucher');
        $conditions = Mage::getSingleton('giftvoucher/giftvoucher')->getConditions();
        $quote = $observer->getEvent()->getQuote();
        $quote->setQuote($quote);
        foreach ($collection as $item) {
            if (in_array($item->getGiftCode(), $addedCodes)) {
                continue;
				
            }
            if ($item->getConditionsSerialized()) {
                $conditionsArr = unserialize($item->getConditionsSerialized());
                if (!empty($conditionsArr) && is_array($conditionsArr)) {
                    $conditions->setConditions(array())->loadArray($conditionsArr);
                    if (!$conditions->validate($quote)) {
                        continue;
                    }
                }
            }
		
         if($item->getFreeShipping() == "1") 
			{
				
				$address = $quote->getShippingAddress();
				$address->setShippingMethod('freeshipping_freeshipping');
			}
			
        }
/* 	 if($item->getGiftvoucherComments() == "Free shipping") 
	{
		$address = $quote->getShippingAddress();
		$address->setShippingMethod('freeshipping_freeshipping');
	} */
	
	//$session = Mage::getSingleton('checkout/session');
	//var_dump($session->getGiftCodes());
	} 

 }

