<?php
class Atak_Voucher_IndexController extends Mage_Core_Controller_Front_Action
{
	
	public function indexAction() {    
       
		$this->loadLayout();
		$this->renderLayout();
    }
	public function activateAction(){
		$coupon_code = $this->getRequest()->getParam('coupon_code');
		//If coupon code is not blank
        if($coupon_code != "")        {
			$storeId = Mage::app()->getStore()->getStoreId();
			$websiteId = Mage::getModel('core/store')->load($storeId)->getWebsiteId();
			$customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
			$_rules= Mage::getResourceModel('salesrule/rule_collection')
				->setValidationFilter($websiteId, $customerGroupId, $coupon_code)
				->load();
			if(count($_rules) > 0) {
				$coupon = Mage::getModel('salesrule/coupon')->load($coupon_code, 'code');
				if($coupon->getTimesUsed() < $coupon->getUsageLimit()) {
					$couponUsage = new Varien_Object();
					$customerId = Mage::getSingleton('customer/session')->getCustomerId();
					Mage::getResourceModel('salesrule/coupon_usage')->loadByCustomerCoupon($couponUsage, $customerId, $coupon->getId());
					if ($couponUsage->getCouponId() && $couponUsage->getTimesUsed() >= $coupon->getUsagePerCustomer()){
							$this->_initLayoutMessages('customer/session');
							$message = $this->__('This voucher is already used by this user');
							Mage::getSingleton('core/session')->addError($message);
							session_write_close();
							$this->_redirect('voucher');
                        } else {
							Mage::getSingleton("checkout/session")->setData("coupon_code",$coupon_code);
							Mage::getSingleton('checkout/cart')->getQuote()->setCouponCode($coupon_code)->save();
							$this->_redirect('voucher/index/activated');
						}
					
				} else {
					$this->_initLayoutMessages('customer/session');
					$message = $this->__('This voucher is already used');
					Mage::getSingleton('core/session')->addError($message);
					session_write_close();
					$this->_redirect('voucher');
				}
			} else {
				$this->_initLayoutMessages('customer/session');
				$message = $this->__('Sorry there seems to be a problem with this coupon code.');
				Mage::getSingleton('core/session')->addError($message);
				session_write_close();
				$this->_redirect('voucher');
			
			}
	        
        }
		
	}
	public function activatedAction() {    
       
		$this->loadLayout();
		$this->renderLayout();
    }
}
?>