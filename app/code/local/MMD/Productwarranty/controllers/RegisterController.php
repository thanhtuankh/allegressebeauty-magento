<?php
class MMD_Productwarranty_RegisterController extends Mage_Core_Controller_Front_Action
{
public function preDispatch()
		{
			parent::preDispatch();
			$action = $this->getRequest()->getActionName();
			
			if(Mage::getStoreConfig('productwarranty/general/enabled_guest_registration')==0)
			{
				$loginUrl = Mage::helper('customer')->getLoginUrl();
				
				if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
					$this->setFlag('', self::FLAG_NO_DISPATCH, true);
				}
			}
		}   
    public function indexAction()
    {
    	
    			
		$this->loadLayout();     
		$this->renderLayout();
    }
	public function postAction()
    {
        $post = $this->getRequest()->getPost();
		$cid = Mage::getSingleton('customer/session')->getCustomer()->getId();
        if ( $post ) {
			
		if($post['state_id']) { 
		
		$region = Mage::getModel('directory/region')->load($post['state_id']);		
		$post['state'] = $region->getName(); } elseif($post['state']!="") { $post['state'] = $post['state']; }
		else
		{
		unset($post['state']); 
		}		
    unset($post['state_id']);    
	
		if($cid) { $post['cid'] = $cid; }
		 
            try {                
                $error = false;
				if (!Zend_Validate::is(trim($post['title']) , 'NotEmpty')) {
                    $error = true;
                }
				
                if (!Zend_Validate::is(trim($post['firstname']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['lastname']) , 'NotEmpty')) {
                    $error = true;
                }
				if (!Zend_Validate::is(trim($post['address1']) , 'NotEmpty')) {
                    $error = true;
                }
				 if (!Zend_Validate::is(trim($post['city']) , 'NotEmpty')) {
                    $error = true;
                }
				
				 if (!Zend_Validate::is(trim($post['country']) , 'NotEmpty')) {
                    $error = true;
                }
				if (!Zend_Validate::is(trim($post['postcode']) , 'NotEmpty')) {
                    $error = true;
                }
				if (!Zend_Validate::is(trim($post['country']) , 'NotEmpty')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }
				 if (!Zend_Validate::is(trim($post['telephone']) , 'NotEmpty')) {
                    $error = true;
                }
             

                if ($error) {
                    throw new Exception();
                }
			
			


			
			//save data
			$post['created_time']= Mage::getModel('core/date')->timestamp(time());
			$model = Mage::getModel('productwarranty/productwarranty');
			$model->addData($post);
			$model->save();

			$lastId = $model->getId();			
			
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			$connection->beginTransaction();
			
			$k=0;
			foreach($post['hide'] as $row)
			{	if($row==1)
				{			
					$productData = Mage::getModel('catalog/product')->load($post['product_id'][$k]);	
					$fields = array(); 
					$fields['pw_id'] = $lastId;
					$fields['product_id']= $post['product_id'][$k];
				    if($cid) { $fields['customer_id']= $cid;}
					$fields['product_name']= $productData->getName();
					$fields['product_sku']= $productData->getSku();
					$fields['serial_number']= $post['serial_number'][$k];
					$fields['purchase_date']= date("Y-m-d",strtotime($post['purchase_date'][$k]));
					$fields['purchased_from']= $post['purchased_from'][$k];
					$connection->insert('productwarranty_products', $fields);
					$connection->commit();
				}
				$k++;
			}
			
			

			
			if(Mage::getStoreConfig('productwarranty/general/product_warranty_email'))
			{
				$to = Mage::getStoreConfig('productwarranty/general/product_warranty_email');
			} else {
				$to = Mage::getStoreConfig('trans_email/ident_general/email');
			}
			
$skinUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'frontend/default/default/';
$body = '
<br>
<img src="'.$skinUrl.'images/logo.gif" alt="Logo">
<h4>Customer Information</h4>
<hr>
<table cellspacing="0" cellpadding="0" border="0" style="width: 510px;">
        <tbody><tr id="ctl00_contentMain_uctlContact_countryRow">
			<td style="width:30%; padding: 0px 6px 12px 0px; text-align: right;color:#333;">
                Name: 
            </td>
			<td style="width: 70%; padding: 0px 6px 12px 0px;">'.$post['title'].' '.$post['firstname'].' '.$post['lastname'].'</td>
		</tr>
		<tr id="ctl00_contentMain_uctlContact_pnlCompany">
			<td style="width:30%; padding: 6px 6px 6px 0px; text-align: right;color:#333;">Email: </td>
			<td style="width: 70%; padding: 6px 0px;">'.$post['email'].'</td>
		</tr>
        <tr>
            <td style="width:30%; padding: 0px 6px 6px 0px; text-align: right;color:#333;">
                Address: 
            </td>
            <td style="width:70%; padding: 0px 0px 6px 0px;">'.$post['address1']." ".$post['address2'].'<br/>'.$post['city'].', '.$post['state'].', '.$post['country'].' '.$post['postcode'].'</td>
        </tr>
        
       <tr id="ctl00_contentMain_uctlContact_pnlCompany">
			<td style="width:30%; padding: 6px 6px 6px 0px; text-align: right;color:#333;">Telehone: </td>
			<td style="width: 70%; padding: 6px 0px;">'.$post['telephone'].'</td>
		</tr>
		<tr id="ctl00_contentMain_uctlContact_pnlCompany">
			<td style="width:30%; padding: 6px 6px 6px 0px; text-align: right;color:#333;">Mobile: </td>
			<td style="width: 70%; padding: 6px 0px;">'.$post['mobile'].'</td>
		</tr>
		<tr id="ctl00_contentMain_uctlContact_pnlCompany">
			<td style="width:30%; padding: 6px 6px 6px 0px; text-align: right;color:#333;">Fax: </td>
			<td style="width: 70%; padding: 6px 0px;">'.$post['fax'].'</td>
		</tr>
    </tbody></table><h4>Product Information</h4><hr><table cellspacing="1" cellpadding="1" border="0" style="border:1px solid #ccc;width: 610px;">';

	$body .=	"<tr bgcolor='#CCCCCC'><td><strong>Product</strong></td><td><strong>Serial#</strong></td><td><strong>Date of Purchase</strong></td><td><strong>Purchased From</strong></td></tr>";
	if(count($post['hide'])>0)
	{    $i=0;
		foreach($post['hide'] as $row)
		{	if($row==1)
			{
			$product = Mage::getModel('catalog/product')->load($post['product_id'][$i]);		
			
			$body .=	"<tr bgcolor='#EFFFFD'><td>".$product->getName()."</td><td>".$post['serial_number'][$i]."</td><td>".$post['purchase_date'][$i]."</td><td>".$post['purchased_from'][$i]."</td></tr>";
			}
			
			$i++;
		}
	}
$body .=	"</table>";
				
				$subject = 'Product Warranty Registration';	
				
				$mail = new Zend_Mail();
				//$mail->setBodyText($rq_msg);
				$mail->setBodyHtml($body);
				$mail->setFrom($post['email'], $post['firstname']." ".$post['lastname']);
				$mail->addTo($to, 'Some Recipient');
				$mail->setSubject($subject);
				$mail->send(); 
			
				
               // Mage::getSingleton('core/session')->addSuccess('Your product warranty registration has been submitted for processing successfully.');
               // $this->_redirect('*/*/');
			   $this->_redirect('warranty-thank-you');
				
                return;
            } catch (Exception $e) {
               // $translate->setTranslateInline(true);
                Mage::getSingleton('core/session')->addError('Unable to submit your request. Please, try again later');
                $this->_redirect('*/*/');
                return;
            }

        } else {
            $this->_redirect('*/*/');
        }
    }
	
}