<?php
class MMD_Productwarranty_Block_Register extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getProductwarranty()     
     { 
        if (!$this->hasData('productwarranty')) {
            $this->setData('productwarranty', Mage::registry('productwarranty'));
        }
        return $this->getData('productwarranty');
        
    }
}