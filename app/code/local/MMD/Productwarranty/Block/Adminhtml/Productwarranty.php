<?php
class MMD_Productwarranty_Block_Adminhtml_Productwarranty extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_productwarranty';
    $this->_blockGroup = 'productwarranty';
    $this->_headerText = Mage::helper('productwarranty')->__('Product Warranty Registrations');
    //$this->_addButtonLabel = Mage::helper('productwarranty')->__('Add Item');
    parent::__construct();
	$this->_removeButton('add');	
  }
}