<?php

class MMD_Productwarranty_Block_Adminhtml_Productwarranty_View extends Mage_Adminhtml_Block_Widget_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'productwarranty';
        $this->_controller = 'adminhtml_productwarranty';      
		
    }
   
}