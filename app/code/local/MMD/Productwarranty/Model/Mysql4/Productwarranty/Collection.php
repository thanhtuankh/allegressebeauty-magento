<?php

class MMD_Productwarranty_Model_Mysql4_Productwarranty_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('productwarranty/productwarranty');
    }
	public function getSelectCountSql()
{
    $countSelect = parent::getSelectCountSql();

    //added 
    $countSelect->reset(Zend_Db_Select::GROUP);
    //end

    $countSelect->resetJoinLeft();
    return $countSelect;
}
}