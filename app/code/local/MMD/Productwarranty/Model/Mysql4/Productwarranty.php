<?php

class MMD_Productwarranty_Model_Mysql4_Productwarranty extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the productwarranty_id refers to the key field in your database table.
        $this->_init('productwarranty/productwarranty', 'productwarranty_id');
    }
}