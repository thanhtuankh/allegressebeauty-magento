<?php
/**
 * CommerceLab Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the CommerceLab License Agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://commerce-lab.com/LICENSE.txt
 *
 * @category   CommerceLab
 * @package    CommerceLab_News
 * @copyright  Copyright (c) 2012 CommerceLab Co. (http://commerce-lab.com)
 * @license    http://commerce-lab.com/LICENSE.txt
 */

class CommerceLab_News_Block_News extends CommerceLab_News_Block_Abstract
{
    protected function _prepareLayout()
    {
        if ($head = $this->getLayout()->getBlock('head')) {
            // show breadcrumbs
            $moduleName = $this->getRequest()->getModuleName();
            $showBreadcrumbs = (int)Mage::getStoreConfig('clnews/news/showbreadcrumbs');
            if ($showBreadcrumbs && ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) && ($moduleName=='clnews')) {
                $breadcrumbs->addCrumb('home',
                    array(
                    'label'=>Mage::helper('clnews')->__('Home'),
                    'title'=>Mage::helper('clnews')->__('Go to Home Page'),
                    'link'=> Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)));
                $newsBreadCrumb = array(
                    'label'=>Mage::helper('clnews')->__(Mage::getStoreConfig('clnews/news/title')),
                    'title'=>Mage::helper('clnews')->__('Return to ' .Mage::helper('clnews')->__('News')),
                    );
                if ($this->getCategoryKey()) {
                    $newsBreadCrumb['link'] = Mage::getUrl($this->getAlias());
                }
                $breadcrumbs->addCrumb('news', $newsBreadCrumb);

                if ($this->getCategoryKey()) {
                    $categories = Mage::getModel('clnews/category')
                        ->getCollection()
                        ->addFieldToFilter('url_key', $this->getCategoryKey())
                        ->setPageSize(1);
                    $category = $categories->getFirstItem();
                    $breadcrumbs->addCrumb('category',
                        array(
                        'label'=>$category->getTitle(),
                        'title'=>Mage::helper('clnews')->__('Go to Home Page'),
                        ));
                }
            }
            // set default meta data
            $head->setTitle(Mage::getStoreConfig('clnews/news/metatitle'));
            $head->setKeywords(Mage::getStoreConfig('clnews/news/metakeywords'));
            $head->setDescription(Mage::getStoreConfig('clnews/news/metadescription'));

            // set category meta data if defined
            $currentCategory = $this->getCurrentCategory();
            if ($currentCategory!=null) {
                if ($currentCategory->getTitle()!='') {
                    $head->setTitle($currentCategory->getTitle());
                }
                if ($currentCategory->getMetaKeywords()!='') {
                    $head->setKeywords($currentCategory->getMetaKeywords());
                }
                if ($currentCategory->getMetaDescription()!='') {
                    $head->setDescription($currentCategory->getMetaDescription());
                }
            }
        }
    }
}
